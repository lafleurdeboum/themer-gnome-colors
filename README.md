# themer-gnome-colors
This is a colors theme for [themer](https://github.com/mjswensen/themer). It
pulls the colors of the Gnome desktop and gives that as input to themer.

Practically, it does the exact inverse from
[themer-gnome-terminal](https://github.com/agarrharr/themer-gnome-terminal),
that is : pull the colors from gnome-terminal, assuming they use the gnome
defaults, and bake a color selection out of it.

## Goal
Get a step closer to unified colors in a linux desktop.

## Limitations
For now it only returns a dark theme.

Subjectively, the Gnome color set is not very handsome in console. And the
blue color used to render directories in `ls` is definitely too dark.

Contributions and comments welcome !

## Installation & usage
Install this module wherever you have `themer` installed:

    npm install themer-gnome-colors

Then pass `themer-gnome-colors` as a `-c` (`--colors`) argument to `themer`:

    themer -c themer-gnome-colors ...

See the [themer](https://github.com/mjswensen/themer) page for detailed
instructions.

## License
This code is licensed under the GPLv2 license, see LICENSE.md attached.
