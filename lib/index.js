const spawn = require('child_process').spawnSync;

let id = spawn(
  'dconf',
  ['read', '/org/gnome/terminal/legacy/profiles:/default']
).stdout.toString('utf8').replace(/'/g, '').replace(/\n/, '');

let palette = spawn(
  'dconf',
  ['read', '/org/gnome/terminal/legacy/profiles:/:' + id + '/palette']
).stdout.toString('utf8');

let colors = palette.replace(/\[/, '').replace(/\]/, '').split(', ');
colors = colors.map(item => item.replace(/'/g, '').replace(/\n/, ''));

module.exports.colors = {
  dark: {
    accent0: colors[1],
    accent1: colors[9],
    accent2: colors[3],
    accent3: colors[2],
    accent4: colors[6],
    accent5: colors[4],
    accent6: colors[5],
    accent7: colors[13],
    shade0: colors[0],
    shade7: colors[15],
  }
}
